import requests as req
import json

#q = get_csdd_data('tesla')

def get_open_data(query, type='ta'):
    opts = {'ta':'&resource_id=f2579b7a-a752-43b7-8d7a-6924daac5e09',
    'reg':'&resource_id=ef249dca-dee5-4b4b-8ae1-3adb9c140387'}
    base_link = 'https://data.gov.lv/dati/lv/api/3/action/datastore_search?q='
    res_id = opts[type]
    res = req.get(base_link+query+res_id)
    res = res.content.decode('utf-8')
    res = json.loads(res)
    return(res)


def ta_mark_freq(res, mark=0):
    cnt=0
    sum_=0
    for r in res['result']['records']:
        mark_ = int(r['Novertējums'])
        sum_ = sum_ + mark_
        if mark_ == mark:
            cnt=cnt+1
    rec_cnt = len(res['result']['records'])
    freq=cnt/rec_cnt
    avg=sum_/rec_cnt
    return({'freq':freq, 'avg':avg})


def reg_data_cnt(res):
    res = len(res['result']['records']) 
    return(res)



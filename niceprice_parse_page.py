import requests as req
from bs4 import BeautifulSoup
import pandas as pd
import re as re
import datetime as dt

#for debug: link = 'https://www.ss.lv/msg/lv/transport/cars/jaguar/s-type/cebbg.html'

def get_ad_opts(link):
    pg = req.get(link)
    s = BeautifulSoup(pg.content)
    #result placeholder
    res = dict()
    res['link'] = link
    #find price:
    price = s.find('td', attrs={'class':'ads_price'}).text.strip()
    price = ''.join(re.findall(r'[0-9]', price))
    res.update({'Cena':price})

    #find less detailed opts:
    table = s.find('table', attrs={'class':'options_list'})
    rows = table.find_all('tr')
    link_list = link.split('/')

    for r in rows:
        attr = r.find('td', attrs={'class':'ads_opt_name'}).text.strip()
        val = r.find('td', attrs={'class':'ads_opt'}).text.strip()
        res.update({attr: val})
    
    for i in range(len(link_list)):
        if(link_list[i])=='cars':
            res.update({'Marka': link_list[i+1]})
            res.update({'Modelis': re.sub('-',' ',link_list[i+2])})

    return(res)

def convert_ad_opts(dic):
    res = dict()
    res['link'] = dic['link']
    res['Cena'] = dic['Cena']
    res['Gads'] = re.findall(r'\w[0-9]+\w',dic['Izlaiduma gads:'])[0]

    res['Modelis'] = dic['Modelis']
    res['Marka'] = dic['Marka']

    try:
        res['Tilp'] = ''.join(re.findall(r'[0-9\.]',dic['Motors:']))
        res['Tips'] = re.sub(r'[0-9\.]','', dic['Motors:'])[1].upper()
    except:
        print('No Motors, probably electric.')
        res['Tilp'] = ''
        res['Tips'] = 'E'

    try:
        res['Karba'] = dic['Ātr.kārba:'][0]
        res['Atrumi'] = re.findall(r'[0-9]',dic['Ātr.kārba:'])[0]
    except:
        print('No karba')
    
    try:
        res['Virsbuve'] = dic['Virsbūves tips:'][0]
    except:
        print('No virsbuve')
    
    try:
        res['Nobrauk'] = int(''.join(re.findall('[0-9]', dic['Nobraukums, km:']))) / 1000
    except:
        print("No nobraukums.")
        res['Nobrauk'] = ''
    try:
        res['Krasa'] = dic['Krāsa:']
    except:
        print('No krasa.')
    try:
        res['TA'] = dic['Tehniskā apskate:']
    except:
        print('No TA.')
    try:
        res['Vietas'] = dic['Vietu skaits:']
    except:
        print('No vietu skaits.')
    
    res['hasVIN'] = 'VIN kods:' in dic
    res['hasPlates'] = 'Valsts numura zīme:' in dic

    if ('published') not in dic:
        print('Published not in dic, using now.')
        res['published'] = str(dt.datetime.now())

    return(res)



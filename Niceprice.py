import streamlit as st
import pandas as pd
import niceprice_parse_page as nparse_page
import niceprice_predict as npredict
import ta_data_portal_api as csdd
import feedparser as fp
import re as re
import random
from datetime import datetime

#cosmetic changes to default template
hide_menu_style = """<style>#MainMenu {visibility: hidden;}</style>"""
st.markdown(hide_menu_style, unsafe_allow_html=True)


#cache period
cacheper = datetime.today().strftime('%Y-%m-%d')


def predict_ss(link):
    ad = nparse_page.convert_ad_opts(nparse_page.get_ad_opts(link))
    df = pd.DataFrame.from_dict(ad, orient='index').transpose()
    model = npredict.load_model()
    df = npredict.get_niceprice(df, model)
    df = df.fillna('')
    return(df)

@st.cache
def get_feed(cacheper):
    try:
        feed = fp.parse('https://www.ss.lv/lv/transport/cars/rss/')
        df = pd.DataFrame(parse_feed(feed))
        li  = list(df['link'])
    except:
        li = ['links uz sludinājumu šeit']
    return(li)

def parse_feed(feed, modified=None):
    res_updated = feed.updated
    feed_entries = feed.entries
    res = list()
    for f in feed_entries:
        res_dt = f.published
        res_link = f['links'][0]['href']
        val = f.summary_detail.value
        try:
            res_i = parse_feed_value(val)
            res_i.update({'updated':res_updated, 
                    'published':res_dt, 
                    'link':res_link})
        except:
            print('Error in parsing ' + val)
        if (res_i['Cena'].isnumeric() and ('Marka' in res_i)): #'perku' is not numeric
            res.append(res_i)
    return(res)

def parse_feed_value(value):
    value = re.sub('<b><b>', '<b>', value)
    value = re.sub('</b></b>', '</b>', value)
    parsed = re.split('<b>+|</b>|<br>|</br>|</a>', value)
    interest = ['Marka', 'Modelis', 'Gads', 'Tilp',  'Nobrauk', 'Cena']
    res=dict()
    for i in enumerate(parsed):
        for c in interest:
            match = len(re.findall(c,i[1]))
            if (match > 0):
                val = parsed[i[0]+1]
                val = re.sub(' tūkst.|  €|,','', val)
                res.update({c:val})
    
    #just a bit of conversion on the fly: engine 
    et = lambda x: re.sub('[0-9]|\\.','',x) if (re.search(r'[A-Z]',x)) else 'B'
    engine_type = et(res['Tilp'])
    res.update({'Tips': engine_type})

    #and engine type
    en = lambda x: re.sub('[a-zA-Z]','',x) if (x!='E') else 0
    engine = en(res['Tilp'])
    res.update({'Tilp': engine})

    #if (res['Cena'].isnumeric() and ('Marka' in res)):
    return(res)


texts = dict()
texts.update({'0':'Sludinājumā norādītā cena ir daudz mazāka par vidējo šādam auto, tas var nozīmēt, ka stāvoklis ir sliktāk par vidējo.'})
texts.update({'1':'Pārdošanas cena ir zemāka par vērtējumu. Auto var būt daži sīki defekti, vai pārdevējs vēlas auto pārdot ātri.'})
texts.update({'2':'Sludinājumā norādītā cena atbilst vērtējumam.'})
texts.update({'3':'Cena ir daudz augstāka par par vidējo, stāvoklim jābūt tuvu ideālam un komplektācijai ļoti labai.' })

def rate(ratio):
    if ratio < 0.7:
        res = 0
    elif ratio < 0.9:
        res = 1
    elif ratio < 1.2:
        res = 2
    else:
        res = 3
    return(res)

def get_pred_output(df):
    df = df.to_dict('records')[0]

    pred = int(df['nice'])
    cena  = int(df['Cena'])
    ratio = cena / pred
    rating = rate(ratio)
    
    make = df['make']
    model = df['model_orig']

    res = f'{make.upper()} {model.upper()} vērtējums ir **{pred}EUR**, bet sludinājumā norādītā cena \
         ir **{cena}EUR**. {texts[str(rating)]}'

    res_text_post = 'Dati par nobraukumu nav pieejami, vērtējums būs mazāk precīzs.' if df['mileage']=='' else ''

    res = ' '.join([res, res_text_post])

    return(st.markdown(res))

def get_csdd_ta_output(df):
    df = df.to_dict('records')[0]
    
    make = df['make']
    make = re.sub('volkswagen','vw',make) #csdd uses vw
    make = re.sub('skoda', 'škoda', make) #same reason as above
    make = re.sub('-', ' ', make)

    model = df['model_orig']
    model = re.sub('\sb[0-9]$','', model)
    model = re.sub('\s[0-9]$', '', model)
    

    try:
        ta_data = csdd.get_open_data(make+' '+model, 'ta')
        ta_stats = csdd.ta_mark_freq(ta_data, mark=0)
        ta_avg_rating = ta_stats['avg']
        ta_mark_freq = ta_stats['freq']
        rating_text_post = f'Ir jāapskata vismaz **{int(round(1/ta_mark_freq,1))+1}** varianti,\
            lai atrastu auto teicamā stāvoklī.' if  ta_mark_freq > 0 else 'Atrast auto teicamā stāvoklī būs ļoti grūti'
        res = f'Saskaņā ar CSDD, **{int(round(ta_mark_freq,1)*10)}** no **10** {make.upper()} {model.upper()}  \
            ir ieguvuši atzīmi 0 tehniskajā apskatē. Vidējā tehniskās apskates atzīme ir **{round(ta_avg_rating,1)}** \
            (mazāk ir labāk).'
        res = ' '.join([res, rating_text_post])
    except:
        res = f'Nav izdevies iegūt datus no CSDD par {make.upper()} {model.upper()}'
    return(st.markdown(res))

def get_csdd_reg_output(df):
    df = df.to_dict('records')[0]
    
    make = df['make']
    year = str(df['year'])
    make = re.sub('volkswagen','vw',make) #csdd uses vw
    make = re.sub('skoda', 'škoda', make) #same reason as above
    make = re.sub('-', ' ', make)

    model = df['model_orig']
    model = re.sub('\sb[0-9]$','', model)
    model = re.sub('\s[0-9]$', '', model)

    try:
        data = csdd.get_open_data(make+' '+model+' '+year, 'reg')
        rating = csdd.reg_data_cnt(data)
        rating_text_pre = 'vairāk nekā' if rating > 99 else ''
        rating_text_post = 'Ir gan pieprasījums, gan piedāvājums uz šādiem auto, \
            nebūs problēmu ar tā pārdošanu.' if rating > 99 else 'Pārdot šādu auto varētu aizņemt vairāk laika.'
        res = f'{make.upper()} {model.upper()}  pagajušajā gadā tika piereģistrēti {rating_text_pre} **{rating}** reizes.'
        res = ' '.join([res,rating_text_post])
    except:
        res = f'Nav izdevies iegūt datus no CSDD par {make.upper()} {model.upper()} reģistrācijām'
    return(st.markdown(res))



######## interface

st.markdown('<h1>Niceprice <sup>beta</sup></h1>',unsafe_allow_html=True)

'''
Iekopē saiti uz auto sludinājumu un iegūsti cenas reitingu.
'''
#input box
feed = get_feed(cacheper)
#link = st.text_input("", feed.pop())
link = st.text_input("", feed[0])

#for debug
#link = 'https://www.ss.lv/msg/lv/transport/cars/bmw/535/bnjod.html'

#main prediction result
st.markdown('---')
if link:
    link = re.sub('m.ss.lv', 'www.ss.lv', link)
    link = re.sub('/msg/ru/','/msg/lv/', link)
    link = re.sub('ss.com', 'ss.lv', link)
    if not (re.match('https://www.ss.lv/msg/lv/transport/cars/', link)
        or re.match('https://m.ss.lv/msg/lv/transport/cars/', link)):
        st.warning('Pašlaik tikai vieglo auto sludinājumi no ss.lv.')
    else:
        df = predict_ss(link)
        get_pred_output(df)


#st.table(df)

#csdd TA information
st.markdown('---')
try:
    get_csdd_ta_output(df)
except:
    st.markdown('Nav izdevies iegūt TA datus no CSDD.')

#csdd registration information
st.markdown('---')
try:
    get_csdd_reg_output(df)
except:
    st.markdown('Nav izdevies iegūt reģistrāciju datus no CSDD.')



#table with links
st.markdown('---')
feed = feed[0:5]
st.markdown('{} nesen ievietotie sludinājumi:'.format(len(feed)))

for i in feed:
    st.markdown(i)

#some static text
st.markdown('---')
memo = '\
Vērtējumam izmantoti [vēsturiskie dati](https://www.kaggle.com/dimalvov0/cars-aging-classifieds) \
par auto sludinājumiem Latvijā un [catboost](https://catboost.ai/) mašīnmacīšanās algoritms.    \
Vērtējumu vidējā kļūda ir no 6% (jaunākiem auto) līdz 20% (vecākiem auto). \
Dati par tehniskajām apskatēm no CSDD caur [atvērto datu portālu](https://data.gov.lv/lv).'
st.markdown(memo)

st.markdown("""<img src="https://www.google-analytics.com/collect?v=1&tid=UA-148836625-2&cid=555&aip=0&t=event&ec=pageload&ea=open&dp=0&dt=0">""",
unsafe_allow_html=True)



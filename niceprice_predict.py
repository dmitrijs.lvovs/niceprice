import pandas as pd 
import pickle 
import re
import catboost
import os

#model = pickle.load(open('./cars.pickle.dat', 'rb'))
#df_data = pd.read_csv('./ssfeed.csv')


def reduce_model(row):
    if(row['make'].lower()=='bmw'):
        if(re.match('[0-9]',row['model'])):
            row['model'] = row['model'][0] 
    elif(row['make'].lower()=='mercedes'):
        row['model']=re.sub('[0-9]','',row['model'])
    return(row)
        

def transform_feed_data(df):
    df['year'] = pd.to_numeric(df['Gads'])
    df['engine'] = df['Tilp']
    df['mileage'] = df['Nobrauk']
    df['fuel_type'] = df['Tips']
    df['model'] = df['Modelis']
    df['make'] = df['Marka']

    df['ad_year'] = pd.DatetimeIndex(pd.to_datetime(df['published'])).year
    df['age'] = df['ad_year'] - df['year']

    df['Cena'] = pd.to_numeric(df['Cena'])

    df['model_orig'] = df['model']

    df = df.fillna('')

    df=df.apply(lambda x: reduce_model(x), axis=1)
    #convert to model columns names

    cols=['year', 'engine', 'mileage', 'fuel_type', 
        'age', 'make', 'model','Cena', 'link', 'model_orig']
    
    df = df.applymap(lambda x: x.lower() if isinstance(x, str) else x)

    return(df[cols])

def dummize_adapt_predict(df, model):
    if (str(type(model))=="<class 'catboost.core.CatBoostRegressor'>"):
        df['fuel_type'].apply(lambda x: re.sub('B','P', x))
        niceprice = model.predict(df[model.feature_names_])
    else:
        res = pd.get_dummies(df)
        res['fuel_type_P'] = res['fuel_type_B']
        df_model = pd.DataFrame(columns = model.feature_names)
        res = pd.concat([res, df_model], axis=0).fillna(0)
        cols = df_model.columns
        res = res[cols]
        if(str(type(model))=="<class 'xgboost.core.Booster'>"):
            res = xgb.DMatrix(res)
        niceprice=pd.Series(model.predict(res))
    df['niceprice']=niceprice
    #df['nice'] = 1.0 - pd.to_numeric(df['Cena'])/pd.to_numeric(df['niceprice'])
    df['nice']  =df['niceprice']
    return(df)

def transform_back(df):
    df['Gads'] = df['year']
    df['Tilp'] = df['engine']
    df['Nobrauk'] = df['mileage']
    df['Tips'] = df['fuel_type']
    df['Modelis'] = df['model']
    df['Marka'] = df['make']
    df['Reitings'] = df['Cena']/df['nice']
    return(df)

def get_niceprice(df, model):
    dfc = transform_feed_data(df)
    res = dummize_adapt_predict(dfc,model)
    res = transform_back(res)
    return(res)

def format_rating(price, prediction):
   ratio = prediction/price
   if ratio > 0.7 and ratio < 1.5:
       return(1)

def load_model():
    THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
    mod = pickle.load(open(THIS_FOLDER+'/'+'model/cars.pickle.dat','rb'))
    return(mod)

#get_niceprice(df_data, model)
import pandas as pd
import sklearn as sk
import datetime as dt
import pickle
from scipy import stats
import numpy as np
from sklearn import model_selection


#df = pd.read_csv('~/Documents/vertecar/all_parsed.csv')
#now lets try recent data
#df = get_saved_feed_items(DATABASE_URL)
#df = [i[4] for i in df]
#dfc = pd.DataFrame.from_records(df)
#dfc = dfc.drop('link', axis=1)
#dfc['datetime'] = str(datetime.now())
#save is for later reuse
#dfc.to_csv('newdata150620.csv')

#tra2 (new format)
def transform_vertecar(df):
    df['datetime'] = pd.to_datetime(df['datetime'])
    df['ad_year'] = pd.DatetimeIndex(df['datetime']).year 
    df['Tilp'] = pd.to_numeric(df['Tilp'])
    df['Gads'] = pd.to_numeric(df['Gads'])
    df['Nobrauk'] = pd.to_numeric(df['Nobrauk'])
    df['Vecums'] = df['ad_year'] - df['Gads']
    df['Cena'] = pd.to_numeric(df['Cena'])
    df['targetoutlier'] = abs(stats.zscore(df['Cena']))
    df['mielage_outlier'] = abs(stats.zscore(df['Nobrauk']))
    df['age_outlier'] = abs(stats.zscore(df['Vecums']))
    #df = df[df['age_outlier']< 3]
    df = df.fillna('')
    df = df[df['targetoutlier']<3]
    #df = df[df['mielage_outlier'] <3]
    df = df[df['Vecums'] < 20]
    ofinterest = ['Gads','Modelis','Marka','Tilp','Tips','Karba',
    'Atrumi','Virsbuve','Nobrauk','Krasa']
    target = df['Cena']
    df = df.applymap(lambda x: x.lower() if isinstance(x, str) else x)
    return df[ofinterest], target
    

dfc2, target = transform_vertecar(dfc)

dfc_tr, dfc_te, dfc_targ_tr, dfc_targ_te = sk.model_selection.train_test_split(
    dfc2, target, test_size = 0.2, random_state = 8
)

#catboost!
from catboost import CatBoostRegressor


mcat = CatBoostRegressor(
        cat_features=['Modelis', 'Marka', 'Tips', 'Karba', 'Virsbuve', 'Krasa'],
        depth=10,
        learning_rate=0.1,
        iterations=10000,
        loss_function='RMSE'
)


#catboost simple
mcat.fit(dfc_tr, dfc_targ_tr,
        eval_set=(dfc_te, dfc_targ_te),
        early_stopping_rounds=10)

catpred = mcat.predict(dfc_te)
mcat.feature_names = list(dfc_tr.columns.values)
sk.metrics.mean_absolute_error(catpred, dfc_targ_te)
sk.metrics.median_absolute_error(catpred, dfc_targ_te)

#839 with year outliers, 840 without, 771 on new data, cool, 619 when price outlier removed

pickle.dump(mcat, open('./model/cars.pickle.dat', 'wb'))


#asess various years performance
df_res = dfc_te.copy()
df_res['err'] = abs((catpred - dfc_targ_te) / dfc_targ_te)
df_res['abs_err'] = abs((catpred - dfc_targ_te))

df_res.groupby(['Gads'])['err'].median()
df_res.groupby(['Marka'])['err'].median()
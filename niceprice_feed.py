import feedparser as fp
import pandas as pd
import re


#1.backend
#get the feed
feed = fp.parse('https://www.ss.lv/lv/transport/cars/rss/', modified=None)
#lm = feed.headers['Last-Modified']


def parse_feed(feed, modified=None):
    res_updated = feed.updated
    feed_entries = feed.entries
    res = list()
    for f in feed_entries:
        res_dt = f.published
        res_link = f['links'][0]['href']
        val = f.summary_detail.value
        try:
            res_i = parse_feed_value(val)
            res_i.update({'updated':res_updated, 
                    'published':res_dt, 
                    'link':res_link})
        except:
            print('Error in parsing ' + val)
        if (res_i['Cena'].isnumeric() and ('Marka' in res_i)): #'perku' is not numeric
            res.append(res_i)
    return(res)


def parse_feed_value(value):
    value = re.sub('<b><b>', '<b>', value)
    value = re.sub('</b></b>', '</b>', value)
    parsed = re.split('<b>+|</b>|<br>|</br>|</a>', value)
    interest = ['Marka', 'Modelis', 'Gads', 'Tilp',  'Nobrauk', 'Cena']
    res=dict()
    for i in enumerate(parsed):
        for c in interest:
            match = len(re.findall(c,i[1]))
            if (match > 0):
                val = parsed[i[0]+1]
                val = re.sub(' tūkst.|  €|,','', val)
                res.update({c:val})
    
    #just a bit of conversion on the fly: engine 
    et = lambda x: re.sub('[0-9]|\\.','',x) if (re.search(r'[A-Z]',x)) else 'B'
    engine_type = et(res['Tilp'])
    res.update({'Tips': engine_type})

    #and engine type
    en = lambda x: re.sub('[a-zA-Z]','',x) if (x!='E') else 0
    engine = en(res['Tilp'])
    res.update({'Tilp': engine})

    #if (res['Cena'].isnumeric() and ('Marka' in res)):
    return(res)

#parsed = parse_feed(feed)
#df = pd.DataFrame(parsed)

#df.to_csv('ssfeed.csv', index=False, index_label=None)
